<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// mahasiswa
// Route::get('mahasiswa', function () {
//     $arrMahasiswa = [
//         "Risa Lestari", "Rudi Hermawan", "Bambang Kusumo",
//         "Lisa Permata"
//     ];
//     return view('kuliah.mahasiswa')->with('mahasiswa', $arrMahasiswa);
// })->name('mahasiswa');

// dosen
// Route::get('dosen', function () {
//     $arrDosen = [
//         "Maya Fitrianti, M.M.", "Prof. Silvia Nst, M.Farm.",
//         "Dr. Umar Agustinus", "Dr. Syahrial, M.Kom."
//     ];
//     return view('kuliah.dosen')->with('dosen', $arrDosen);
// })->name('dosen');

// // gallery
// Route::get('gallery', function () {
//     return view('kuliah.gallery');
// })->name('gallery');

// admin
Route::get('admin', function () {
    return view('admin.admin');
});

// info
// Route::get('informasi/{fakultas}/{jurusan}', function ($fakultas, $jurusan) {
//     $data = [$fakultas, $jurusan];
//     return view('informasi')->with('data', $data);
// })->name('info');

// Route::get('/', 'Admin\PageController@index');
// Route::get('/mahasiswa', 'Admin\PageController@tampil');

Route::get('/dosen', 'MahasiswaController@dosen')->name('dosen');
Route::get('/mahasiswa', 'MahasiswaController@mahasiswa')->name('mahasiswa');
Route::get('/gallery', 'MahasiswaController@gallery')->name('gallery');
Route::get('/informasi/{fakultas}/{jurusan}', 'MahasiswaController@info')->name('info');

Route::get('/coba-facade', 'Admin\PageController@cobaFacade');
route::get('/coba-class', 'Admin\PageController@cobaClass');