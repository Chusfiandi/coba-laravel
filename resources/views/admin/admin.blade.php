<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <title>Admin Dashboard</title>
</head>

<body>
    <div class="container text-center mt-3 p-4 bg-white">
        <h1>Halaman Admin</h1>
        <div class="row">
            <div class="col-12">

                @component('layout.alerts',['class'=>'warning','judul'=>'Peringatan'])
                100 data mahasiswa perlu di perbaiki
                @endcomponent

                @component('layout.alerts',['class'=>'danger','judul'=>'Awas'])
                Deadline Project hari ini
                @endcomponent

                @component('layout.alerts',['class'=>'success','judul'=>'Kabar'])
                Cuti 3 Hari
                @endcomponent

            </div>
        </div>
    </div>

    <script src="/js/jquery.min.js"></script>
    {{-- <script src="/js/popper.js"></script> --}}
    <script src="/js/bootstrap.min.js"></script>
</body>

</html>