<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Coba\Foo;

class PageController extends Controller
{
    public function index()
    {
        return "halaman admin";
    }

    public function tampil()
    {
        return "Data mahasiswa Admin";
    }
    public function cobaFacade()
    {
        echo \Illuminate\Support\Str::snake('SedangBelajarLaravelUncover');
        echo "<br>";
        echo \Illuminate\Support\Str::kebab('SedangBelajarLaravelUncover');
    }
    public function cobaClass()
    {
        // $foo = new \App\Http\Controllers\Coba\Foo();
        $foo = new Foo();
        echo $foo->bar();
    }
}